# Pokémon Trainer
Fourth and final assignment for the javascript part of the java fullstack course at Noroff in the Experis Academy program.

In this assignment we had to create a webpage where a user could "catch" pokémon, adding the names of the caught pokémon to the user in the trainer API. Both the trainer app and the trainer API are hosted on Heroku.

## Component tree
A pdf with the component tree for the webpage is located in the "component-tree" folder.

## Heroku link
The trainer webpage is hosted on Heroku:

https://dm-tde-pokemon-trainer.herokuapp.com/

## Authors
Daniel Mossestad

Trym Dammann Ellingsen