import { Trainer } from './../models/trainer.model';
import { environment } from './../../environments/environment.prod';
import { catchError, map, Observable, tap, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from '../utils/common/shared/api.service';

const URL = environment.trainerAPIBaseURL;
const API_KEY = environment.trainerAPI_KEY;

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  constructor(private http: HttpClient, private apiService: ApiService) {}

  /**
   * Create a new user!
   * @param {string} username Username from input form
   * @returns {object} The new user object
   */
  register(username: string): Observable<any> {
    const user = {
      username,
      pokemon: [],
    };
    const headers = this.apiService.createHeaders();
    // Return an Observable
    // trainer response
    return this.http.post<Trainer>(URL, user, { headers }).pipe(
      /* tap((response) => {
        if (response.success === false || response.length < 1) {
          throwError(() => new Error(response.error));
        }
        // tap can not change the response. no return is allowed
      }), */
      map((response: Trainer) => {
        return response;
      }),
      // Catch the error
      // Throw the message we want to display in the component
      catchError((error) => {
        throw error;
      })
    );
  }
}
