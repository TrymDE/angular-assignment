import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, map } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonResponse } from '../models/pokemon.model';
import { TrainerService } from './trainer.service';

const pokemonStorageKey: string = 'stored-pokemon';

const baseURL = environment.pokeAPIBaseURL;
/* From postman / API documents, we know there are (currently) only 1118 pokemon in the API,
so fetching with 2000 gives us all available pokemon. This shouldn't really be hard coded,
but it felt weird doing a fetch to get the actual count, then another fetch with said count
as the limit.
This value can also be changed if we decide to implement pagination to be a page instead. */
const pokemonPerPage = 2000;

const errorMessage = "Couldn't load Pokémon";

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  private _pokemon: Pokemon[] = [];
  private _loading: boolean = false;
  private _errorMsg: string = '';

  constructor(
    private http: HttpClient,
    private trainerService: TrainerService
  ) {
    this._errorMsg = '';
  }

  get pokemon(): Pokemon[] {
    return this._pokemon;
  }

  get loading(): boolean {
    return this._loading;
  }

  get error(): string {
    return this._errorMsg;
  }

  /**
   * Method for fetching all pokemon from the PokeAPI (if not stored in session storage.)
   * After fetching pokemon, marks the ones who have been caught.
   */
  findAllPokemon(): void {
    this._loading = true;

    const savedPokemon = sessionStorage.getItem(pokemonStorageKey);

    if (savedPokemon) {
      this._pokemon = JSON.parse(savedPokemon);
      this._loading = false;
      this.setCaught();
    } else {
      this.http
        .get<PokemonResponse>(baseURL + '/pokemon?limit=' + pokemonPerPage)
        .pipe(
          map((response: PokemonResponse) => {
            return response.results;
          }),
          finalize(() => {
            this._loading = false;
          })
        )
        .subscribe({
          next: (pokemon: Pokemon[]) => {
            this._pokemon = pokemon;
            this.setCaught();
            sessionStorage.setItem(pokemonStorageKey, JSON.stringify(pokemon));
          },
          error: (error) => {
            console.log(error);

            this._errorMsg = errorMessage;
          },
        });
    }
  }

  /**
   * Helper method to mark pokemon as caught
   */
  setCaught() {
    for (const pokemon of this._pokemon.filter((pokemon) => {
      return this.trainerService.caughtPokemon.includes(pokemon.name);
    })) {
      pokemon.caught = true;
    }
  }
}
