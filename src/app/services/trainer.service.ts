import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';
import { ApiService } from '../utils/common/shared/api.service';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _username: string = '';
  private _trainer: Trainer = {
    id: 0,
    username: '',
    pokemon: [],
  };

  get username(): string {
    return this._username;
  }

  set username(username: string) {
    this._username = username;
  }

  get caughtPokemon(): string[] {
    return this._trainer.pokemon;
  }

  set trainer(trainer: Trainer) {
    this._trainer = trainer;
    sessionStorage.setItem(environment.TRAINER_KEY, JSON.stringify(trainer));
  }

  get trainer() {
    return this._trainer;
  }

  constructor(private http: HttpClient, private apiService: ApiService) {
    const storedUser: string =
      sessionStorage.getItem(environment.TRAINER_KEY) ?? '';

    if (storedUser) {
      const json = JSON.parse(storedUser) as Trainer;
      this._trainer = json;
    }
  }

  /**
   * Update both the state and trainer API with the new caught pokemon array
   * @param newCaught An array over pokemon the user has caught
   */
  setCaughtPokemon(newCaught: string[]): void {
    // We don't actually need to use the response, since we set the new caught pokemon here.
    this._trainer.pokemon = newCaught;

    sessionStorage.setItem(
      environment.TRAINER_KEY,
      JSON.stringify(this._trainer)
    );

    const headers = this.apiService.createHeaders();

    this.http
      .patch(
        `${environment.trainerAPIBaseURL}/${this._trainer.id}`,
        { pokemon: newCaught },
        { headers }
      )
      .subscribe({
        error: (error: HttpErrorResponse) => {
          console.log(error);
        },
      });
  }

  /**
   * Removes user from store and clears sessionstorage
   */
  logout() {
    this._trainer = {
      id: 0,
      username: '',
      pokemon: [],
    };
    sessionStorage.clear();
  }
}
