import { TrainerService } from 'src/app/services/trainer.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, tap, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';

const URL = environment.trainerAPIBaseURL;

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private http: HttpClient) {}

  /**
   * Fetches the user based on the username provided
   * @param {string} username
   * @returns {Observable}
   */
  login(username: string): Observable<any> {
    return this.http.get<Trainer[]>(`${URL}?username=${username}`).pipe(
      map((trainers) => trainers.pop()),
      /*  tap((trainer) => {
        if (!trainer) {
          console.log('ERROR IN TAP: ', trainer);

          throwError(() => new Error('Could not find user'));
        }
      }), */
      catchError((error) => {
        throw error;
      })
    );
  }
}
