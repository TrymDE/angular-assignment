import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
})
export class LandingPage implements OnInit {
  private isLogin?: boolean = true;
  private buttonText: string = 'Register';

  get getIsLogin() {
    return this.isLogin;
  }

  get getButtonText() {
    return this.buttonText;
  }

  constructor() {}

  switchBetweenLoginAndRegister(): void {
    this.isLogin = !this.isLogin;
    if (this.isLogin) {
      this.buttonText = 'Register';
    } else this.buttonText = 'Login';
  }

  ngOnInit(): void {}
}
