import { Component, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css'],
})
export class TrainerPage implements OnInit {
  constructor(
    private trainer: TrainerService,
    private pokemon: PokemonService
  ) {
    this.pokemon.findAllPokemon();
  }

  ngOnInit(): void {}

  public get error(): string {
    return this.pokemon.error;
  }

  public get caughtPokemon(): string[] {
    return this.trainer.caughtPokemon;
  }
}
