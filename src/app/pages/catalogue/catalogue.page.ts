import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
})
export class CataloguePage implements OnInit {
  constructor(private pokemonService: PokemonService) {}

  ngOnInit(): void {
    this.pokemonService.findAllPokemon();
  }

  public get error(): string {
    return this.pokemonService.error;
  }

  /**
   * List over all pokemon
   */
  public get pokemonList(): Pokemon[] {
    return this.pokemonService.pokemon;
  }

  /**
   * Check whether the page is loading pokemon from the pokeAPI
   */
  public get loading(): boolean {
    return this.pokemonService.loading;
  }
}
