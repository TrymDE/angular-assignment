export interface Trainer {
  id: Number;
  username: string;
  pokemon: string[];
}
