export interface Pokemon {
  name: string;
  url: string;
  caught?: boolean;
}

export interface PokemonResponse {
  // Link to next "page" of pokemon. Useful for pagination, but not much else.
  next: string;
  results: Pokemon[];
}
