import { LoginService } from './../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { TrainerService } from './../../services/trainer.service';
import { Trainer } from 'src/app/models/trainer.model';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
})
export class LoginFormComponent implements OnInit {
  loginError?: string;
  isLoading: boolean = false;

  constructor(
    private router: Router,
    private trainerService: TrainerService,
    private loginService: LoginService
  ) {}

  ngOnInit(): void {}

  /**
   * Runs the login() in login.service and updates the trainer object in trainer.service
   * @param form
   */
  onLoginSubmit(form: NgForm): void {
    this.loginError = '';
    this.isLoading = true;
    const { username } = form.value;
    this.loginService.login(username).subscribe({
      next: (trainer: Trainer) => {
        console.log('next block: ', trainer);

        if (!trainer) {
          const errorMessage = 'Could not find user';
          this.loginError = errorMessage;
          throw Error(errorMessage);
        }

        this.trainerService.trainer = trainer;
        this.router.navigateByUrl('/catalogue');
      },
      error: (error) => {
        this.loginError = error;
      },
      complete: () => {
        this.isLoading = false;
      },
    });
  }
}
