import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { TrainerService } from 'src/app/services/trainer.service';
import { environment } from 'src/environments/environment';

const catchText: string = 'Catch!';
const releaseText: string = 'Release';

@Component({
  selector: 'app-pokemon-item',
  templateUrl: './pokemon-item.component.html',
  styleUrls: ['./pokemon-item.component.css'],
})
export class PokemonItemComponent implements OnInit {
  @Input() pokemon?: Pokemon;
  pokemonImageUrl: string = '';
  buttonText: string = '';

  constructor(private trainer: TrainerService) {}

  /**
   * Sets the pokemonImageUrl for getting the sprite, then sets whether the button should
   * display "Catch!" or "Release"
   */
  ngOnInit(): void {
    const pokemonUrlArray: string[] | undefined = this.pokemon?.url.split('/');
    // Set the pokemon image url
    if (pokemonUrlArray) {
      this.pokemonImageUrl =
        environment.pokemonImgBaseUrl +
        '/' +
        pokemonUrlArray[pokemonUrlArray.length - 2] +
        '.png';
    }

    // Defaults to catchText, since undefined is same as false
    if (this.pokemon?.caught) {
      this.buttonText = releaseText;
    } else {
      this.buttonText = catchText;
    }
  }

  /**
   * Optional method in case we want to add a details page for pokemon
   */
  onPokemonClick() {
    console.log(this.pokemon?.name, 'was clicked!');
  }

  /**
   * Method for catching / releasing pokemon.
   * Will first change the pokemon object caught status to be opposite
   * Then, either removes the pokemon from trainers caughtPokemon, or adds it.
   */
  onCatchReleaseClick() {
    if (this.pokemon) {
      this.pokemon.caught = !this.pokemon.caught;
      let newPokemon: string[] = [];
      if (this.trainer.caughtPokemon.includes(this.pokemon.name)) {
        // Return a string array containing the caught pokemon without the released pokemon.
        newPokemon = this.trainer.caughtPokemon.filter((pokemon) => {
          return pokemon !== this.pokemon?.name;
        });

        this.buttonText = catchText;
      } else {
        this.buttonText = releaseText;
        newPokemon = [...this.trainer.caughtPokemon];
        newPokemon.push(this.pokemon.name);
      }
      this.trainer.setCaughtPokemon(newPokemon);
    }
  }
}
