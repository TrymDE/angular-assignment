import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonService } from 'src/app/services/pokemon.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-pokemon-collected-list',
  templateUrl: './pokemon-collected-list.component.html',
  styleUrls: ['./pokemon-collected-list.component.css'],
})
export class PokemonCollectedListComponent implements OnInit {
  private _caughtPokemonObj: Pokemon[] = [];

  constructor(
    private pokemonService: PokemonService,
    private trainerService: TrainerService
  ) {
    this._caughtPokemonObj = this.pokemonService.pokemon.filter((pokemon) => {
      return this.trainerService.caughtPokemon.includes(pokemon.name);
    });
  }

  ngOnInit(): void {}

  public get caughtPokemonObj(): Pokemon[] {
    return this._caughtPokemonObj;
  }

  public get username(): string {
    return this.trainerService.trainer.username;
  }
}
