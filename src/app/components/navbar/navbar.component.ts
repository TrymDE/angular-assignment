import { TrainerService } from 'src/app/services/trainer.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
})
export class NavbarComponent implements OnInit {
  get trainerId(): Number {
    return this.trainerService.trainer.id;
  }

  constructor(private trainerService: TrainerService, private router: Router) {}

  ngOnInit(): void {}

  /**
   * Removes the user and reloads page
   */
  onLogoutClick() {
    this.trainerService.logout();
    // Reloads page
    window.location.reload();
  }
}
