import { LoginService } from './../../services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/services/register.service';
import { NgForm } from '@angular/forms';
import { Trainer } from 'src/app/models/trainer.model';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
})
export class RegisterFormComponent implements OnInit {
  registerError: string = '';
  isLoading: boolean = false;
  constructor(
    private router: Router,
    private trainerService: TrainerService,
    private registerService: RegisterService,
    private loginService: LoginService
  ) {}

  ngOnInit(): void {}

  /**
   * Register a new trainer. 
   * @param form The form inputs
   */
  onRegisterSubmit(form: NgForm): void {
    this.isLoading = true;
    this.registerError = '';
    const { username } = form.value;
    this.loginService.login(username).subscribe({
      next: (trainer: Trainer) => {
        if (trainer) {
          const errorMessage = 'User already exists. Try to log in instead';
          this.registerError = errorMessage;
          throw Error(errorMessage);
        }
        this.registerService.register(username).subscribe({
          next: (response: Trainer) => {
            // assume it was successful
            console.log('Register', response);
            this.trainerService.trainer = response;
            this.router.navigateByUrl('/catalogue');
          },
          error: (error) => {
            this.registerService = error;
          },
          complete: () => {
            this.isLoading = false;
          },
        });
      },
    });
  }
}
